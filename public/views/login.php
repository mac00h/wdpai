<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA=Compatible" content="ie=edge">
    <title>Login Page</title>
    <link rel="stylesheet" href="public/css/login.css"/>
</head>
<body>

<form class="login" action="login" method="POST">
    <div class = "messages">
        <?php if(isset($messages)){
            foreach($messages as $message){
                echo $message;
            }
        }
        ?>
    </div>
    <h1>Login</h1>
    <input type="text" name="email" placeholder="Username">
    <input type="password" name="password" placeholder="Password">
    <input type="submit" name="submit" value="Login">
</form>
</body>

