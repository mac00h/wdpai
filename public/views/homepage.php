<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA=Compatible" content="ie=edge">
    <title>Homepage</title>
    <link rel="stylesheet" href="public/css/homepage.css"/>
    <link rel="stylesheet" href="public/css/recommendations.css"/>
</head>

<body>
    <div class="homecontainer">
        <header>
        <input type="text" autocomplete="off" class="search-box"
        placeholder="Search for a city.."/>
        </header>
        <main>
            <section class="location">
                <div class="city"></div>
                <div class="date"></div>
            </section>
                <div class="current">
                    <div class="temp"><span></span></div>
                    <div class="weather"></div>
                    <div class="hi-low"></div>
                </div>
        </main>
        <div id="recommendations"></div>
        <button type="submit" id="btn_submit">Search</button>
    </div>
    <div id="hiddenTempo" style="display: none;"></div>
    <div id="hiddenWeather" style="display: none;"></div>
    <input type="hidden" id='hidden_token'>
    <script src="public/js/homepage.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="public/js/spotify.js"></script>
</body>
