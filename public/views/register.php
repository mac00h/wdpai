<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/register.css">
    <title>Register</title>
</head>

<body>
    <form class="register" action="register" method="POST">
        <div class="messages">
            <?php
            if(isset($messages)){
                foreach($messages as $message) {
                    echo $message;
                }
            }
            ?>
        </div>
        <h1>Register</h1>
        <input type="text" name="email" placeholder="email@email.com">
        <input type="password" name="password" type="password" placeholder="password">
        <input type="password" name="confirmedPassword"  placeholder="confirm password">
        <input type="text" name="name" type="text" placeholder="name">
        <input type="text" name="surname" type="text" placeholder="surname">
        <input type="text" name="phone" type="text" placeholder="phone">
        <input type="submit" name="submit" value="Register">
    </form>
<scirpt type="text/javascript" src=".public/js/register.js"></scirpt>
</body>