//default values
let value1 = 0.1; //min_energy of a track
let value2 = 0.9; //max_energy of a track
let valance1 = 0.1; //min_positivity of a track
let valance2; //max_positivity of a track
let rdGenreSeed;
let rdArtistSeed;
let rdTrackSeed;

let genres = [
    "acoustic","afrobeat","alt-rock","alternative","ambient","anime","black-metal","bluegrass","blues","bossanova","brazil","breakbeat",
    "british","cantopop","chicago-house","children","chill","classical","club","comedy","country","dance","dancehall","death-metal","deep-house",
    "detroit-techno","disco","drum-and-bass","dub","dubstep","edm","electro","electronic","folk","forro","french","funk","garage","german","gospel",
    "goth","grindcore","guitar","happy","hard-rock","hardcore","hardstyle","heavy-metal","hip-hop","holidays","honky-tonk","house","idm","industrial",
    "j-dance","j-idol","j-pop","j-rock","jazz","k-pop","latin","latino","malay","mandopop","metal","metal-misc","metalcore","minimal-techno",
    "movies","mpb","new-age","new-release","opera","pagode","party","philippines-opm","piano","pop","pop-film","post-dubstep","power-pop",
    "progressive-house","psych-rock","punk","punk-rock","r-n-b","rainy-day","reggae","reggaeton","road-trip","rock","rock-n-roll","rockabilly","romance",
    "sad","salsa","samba","sertanejo","show-tunes","singer-songwriter","ska","sleep","songwriter","soul","soundtracks","spanish","study",
    "summer","swedish","synth-pop","tango","techno","trance","trip-hop","turkish","work-out","world-music"];

let trackSeed = ["3G0yz3DZn3lfraledmBCT0","7eEEgizmmwDNvdUI7yOiAF","51rXHuKN8Loc4sUlKPODgH","4JpKVNYnVcJ8tuMKjAj50A", "1hWN9FB6Lxnqt2ij8uPH8V", "2NRANZE9UCmPAS5XVbXL40", "0irwK3Hld6BTGAWH9CFjop", "24JygzOLM0EmRQeGtFcIcG", "7JnorPtXApoUw2AuJUGLY1", "7aftSOGSOpSoIlVAQVBb71", "5EWFuo4ObEnfndc57sTuIo", "4UfYbAuL6dpuYWAsunXg9r", "3jVtllWS5CFFWLQng8sKsr"];
let artistSeed =["711MCceyCBcFnzjGY4Q7Un","1yAwtBaoHLEDWAnWR87hBT","13ubrt8QOOCPljQ2FL1Kca","68DWke2VjdDmA75aJX5C57","2YZyLoL8N0Wb9xBt1NhZWg","28ExwzUQsvgJooOI0X1mr3","7dGJo4pcD2V6oG8kP0tJRR","6qqNVTkY8uBg9cP3Jd7DAH", "5IcR3N7QB1j6KBL8eImZ8m", "137W8MRPWKqSmrBGDBFSop", "50hFWG3MaUUX5phNrrVc97", "4MCBfE4596Uoi2O4DtmEMz", "66CXWjxzNUsdJxJ2JdwvnR", "00FQb4jTyendYWaN8pK0wa", "0C8ZW7ezQVs4URX5aX7Kqx"];
const APIController = (function() {
    const clientId = '2fd6d28e9e5f47ccaaec862cb2705848';
    const clientSecret = 'd08365e6f5454548a59891ffee75c520';

    const _getToken = async () => {

        const result = await fetch('https://accounts.spotify.com/api/token', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded', 
                'Authorization' : 'Basic ' + btoa(clientId + ':' + clientSecret)
            },
            body: 'grant_type=client_credentials'
        });

        const data = await result.json();
        return data.access_token;
    }

    const _getRecommendations = async (token, value1, value2, valance1, valance2, rdGenreSeed, rdArtistSeed, rdTrackSeed) => {
        
        const result = await fetch(`https://api.spotify.com/v1/recommendations?limit=5&market=US&seed_artists=${rdArtistSeed}&seed_genres=${rdGenreSeed}&seed_tracks=${rdTrackSeed}&min_energy=${value1}&max_energy=${value2}&min_valence=${valance1}&max_valence=${valance2}`, {
            method: 'GET',
            headers: {
                'Authorization' : 'Bearer ' + token
            }
        });

        const data = await result.json();
        return data.tracks;
    }

    return {
        getToken() {
            return _getToken();
        },
        getRecommendations(token, value1, value2, valance1, valance2, rdGenreSeed, rdArtistSeed, rdTrackSeed) {
            return _getRecommendations(token, value1, value2, valance1, valance2, rdGenreSeed, rdArtistSeed, rdTrackSeed);
        }
    }
})();

const UIController = (function() {

    //object to hold references to html selectors
    const DOMElements = {
        hfToken: '#hidden_token',
        recommendations: '#recommendations',
        albumCover: '#cover'
    }

    //public methods
    return {

        //method to get input fields
        inputField() {
            return {
                recommendations: document.querySelector(DOMElements.recommendations),
                album: document.querySelector(DOMElements.albumCover)
            }
        },
        
        createRecommendations(img, text1, text2, link){
            const html = `<a target="_blank" href=${link}><div class="cover"><img src = ${img}><h2>${text1}</h2><h3>${text2}</h3></div></a>`;
            document.querySelector(DOMElements.recommendations).insertAdjacentHTML('beforeend', html);
        },

        storeToken(value) {
            document.querySelector(DOMElements.hfToken).value = value;
        },

        getStoredToken() {
            return {
                token: document.querySelector(DOMElements.hfToken).value
            }
        }
    }

})();

const APPController = (function(UICtrl, APICtrl) {

    // get input field object ref
    const DOMInputs = UICtrl.inputField();
    const loadRecommendation = async () => {
        const token = await APICtrl.getToken();
        UICtrl.storeToken(token);
        if(valance2 == null){
            alert("Please enter location first!");
        }
        const recomm = await APICtrl.getRecommendations(token, value1, value2, valance1, valance2, rdGenreSeed, rdArtistSeed, rdTrackSeed);
        recomm.forEach(e => UICtrl.createRecommendations(e.album.images[1].url, e.artists[0].name, e.name, e.external_urls.spotify));
    }
    return {
        init() {
            console.log('App is starting');
            loadRecommendation();
        }
    }

})(UIController, APIController);

function getStoredTemperature() {
    const temp = document.getElementById("hiddenTempo").innerText;
    return temp;
}

function getStoredWeatherConditions() {
    const conditions = document.getElementById("hiddenWeather").innerText;
    return conditions;
}

function changeParameter(conditionString, tempInCity) {
    console.log(conditionString);
    console.log(tempInCity);
    if(conditionString == "Clouds"){
        if(tempInCity > -40 && tempInCity < -10){
            valance2 = 0.2;
            value2 = 0.2;
        } else if (tempInCity >= -10 && tempInCity < 0){
            value1 = 0.2;
            value2 = 0.4;
            valance2 = 0.6;
        } else if (tempInCity >= 0 && tempInCity < 10){
            value1 = 0.3;
            value2 = 0.6;
            valance1 = 0.3;
            valance2 = 0.7;
        } else if (tempInCity >= 10 && tempInCity < 20) {
            value1 = 0.5;
            value2 = 0.8;
            valance1 = 0.5;
            valance2 = 0.7;
        } else if (tempInCity >= 20) {
            value1 = 0.1
            value2 = 0.8;
            valance1 = 0.7;
            valance2 = 0.9;
        }
    } else if(conditionString == "Clear"){
        if(tempInCity >= -40 && tempInCity < -5){
            valance2 = 0.6;
            value2 = 0.3;
        } else if (tempInCity >= -5 && tempInCity < 0){
            value1 = 0.2;
            value2 = 0.4;
            valance2 = 0.6;
        } else if (tempInCity >= 0 && tempInCity < 10){
            value1 = 0.3;
            value2 = 0.6;
            valance1 = 0.3;
            valance2 = 0.8;
        } else if (tempInCity >= 10 && tempInCity < 20) {
            value1 = 0.5;
            value2 = 0.8;
            valance1 = 0.3;
            valance2 = 0.7;
        } else if (tempInCity >= 20) {
            value1 = 0.1
            value2 = 0.8;
            valance1 = 0.5;
            valance2 = 0.9;
        }
    } else if(conditionString == "Mist"){
        valance1 = 0.2;
        valance2 = 0.5;
    } else if(conditionString == "Smoke"){
        valance1 = 0.1;
        valance2 = 0.5;
    } else if(conditionString == "Haze"){
        valance1 = 0.2;
        valance2 = 0.6;
    } else if(conditionString == "Dust"){
        valance1 = 0.1;
        valance2 = 0.6;
    } else if(conditionString == "Fog"){
        valance1 = 0.1;
        valance2 = 0.6;
    } else if(conditionString == "Sand"){
        valance1 = 0.2;
        valance2 = 0.6;
    } else if(conditionString == "Dust"){
        valance1 = 0.1;
        valance2 = 0.6;
    } else if(conditionString == "Ash"){
        valance1 = 0.1;
        valance2 = 0.6;
    } else if(conditionString == "Squall"){
        valance1 = 0.1;
        valance2 = 0.4;
    } else if(conditionString == "Tornado"){
        valance1 = 0.1;
        valance2 = 0.3;
    } else if(conditionString == "Snow"){
        valance1 = 0.4;
        valance2 = 0.7;
        value2 = 0.6;
    } else if(conditionString == "Rain"){
        valance1 = 0.2;
        valance2 = 0.6;
        value2 = 0.4;
    } else if(conditionString == "Drizzle"){
        valance1 = 0.2;
        valance2 = 0.6;
        value2 = 0.5;
    } else if(conditionString == "Thunderstorm"){
        valance1 = 0.2;
        valance2 = 0.8;
        value2 = 0.7;
    }
}

function setRandomGenre(){
    const random = Math.floor(Math.random()*genres.length);
    rdGenreSeed = genres[random];
}

function setRandomArtistSeed(){
    const random = Math.floor(Math.random()*artistSeed.length);
    rdArtistSeed = artistSeed[random];
}

function setRandomTrackSeed(){
    const random = Math.floor(Math.random()*trackSeed.length);
    rdTrackSeed = trackSeed[random];
}

function executeSp() {
    APPController.init();
}

//load recommendations when the 'search' button is clicked
$(document).ready(function() {
    $('#btn_submit').on('click', function(e){
        e.preventDefault();
        let tep = getStoredTemperature();
        let condi = getStoredWeatherConditions();
        changeParameter(condi, tep);
        setRandomGenre();
        setRandomArtistSeed();
        setRandomTrackSeed();
        executeSp();
    });
});

